// START GLOBAL
// start add ENTER event to INPUT
var inputMain = document.getElementById("txt-new-element");
inputMain.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    event.preventDefault;
    document.getElementById("add-to-array").click();
  }
});

var inputEx09 = document.getElementById("ex09-so-thuc");
inputEx09.addEventListener("keypress", function (event) {
  if (event.key === "Enter") {
    event.preventDefault;
    document.getElementById("ex09-add-to-array").click();
  }
});
// end add ENTER event to INPUT

// start show array to end user
function showArray(yourArray, whereToShow) {
  var thisArray = yourArray.join(", ");
  document.getElementById(
    whereToShow
  ).innerHTML = `[ <span class="text-black">${thisArray}</span> ]`;
}
// end show array to end user
// END GLOBAL

// START MODIFYING ARRAY - Cho người dùng nhập dữ liệu vào mảng
var mainArray = [];
var mainArrayLocationId = "current-array";
// start adding to array
function addToArray() {
  var element = document.getElementById("txt-new-element").value * 1;
  mainArray.push(element);
  document.getElementById("txt-new-element").value = "";
  document.getElementById(
    "txt-new-element"
  ).placeholder = `Đã thêm [${element}]`;
  //   console.log(mainArray);
  showArray(mainArray, mainArrayLocationId);
}
// end adding to array

// start remove 1 element
function remove1Element() {
  mainArray.pop();
  document.getElementById(
    "txt-new-element"
  ).placeholder = `Nhập một số nguyên bất kỳ - Để trống mặc định: 0`;
  showArray(mainArray, mainArrayLocationId);
}
// end remove 1 element

// start remove array
function removeArray() {
  mainArray.splice(0, mainArray.length);
  document.getElementById(
    "txt-new-element"
  ).placeholder = `Nhập một số nguyên bất kỳ - Để trống mặc định: 0`;
  showArray(mainArray, mainArrayLocationId);
}
// end remove array
// END MODIFYING ARRAY

// START EX01
function tinhTongSoDuong() {
  var tongSoDuong = 0;
  mainArray.forEach(function (n) {
    if (n > 0) {
      tongSoDuong += n;
    }
  });
  //   console.log(tongSoDuong);
  document.getElementById(
    "ex01-result"
  ).innerHTML = `Tổng các số DƯƠNG = <span class="text-warning fs-4 fw-semibold">${tongSoDuong}</span>`;
}
// END EX01

// START EX02
function demSoDuong() {
  var positiveArray = mainArray.filter(function (element) {
    return element > 0;
  });
  if (positiveArray.length == 0) {
    document.getElementById(
      "ex02-result1"
    ).innerHTML = `Mảng <span class="text-warning fw-semibold">KHÔNG</span> có số Dương`;
  } else {
    document.getElementById(
      "ex02-result1"
    ).innerHTML = `Số lượng các số Dương = <span class="text-warning fs-4 fw-semibold">${positiveArray.length}</span>`;
    showArray(positiveArray, "ex02-result2");
  }
}
// END EX02

// START EX03
function timSoNhoNhat() {
  var soNhoNhat = mainArray[0];
  mainArray.forEach(function (n) {
    if (soNhoNhat > n) {
      soNhoNhat = n;
    }
  });
  switch (soNhoNhat) {
    case undefined:
      document.getElementById(
        "ex03-result"
      ).innerHTML = `Mảng <span class="text-warning fw-semibold">RỖNG</span>`;
      break;
    default:
      document.getElementById(
        "ex03-result"
      ).innerHTML = `Số NHỎ NHẤT trong mảng là: <span class="text-danger fw-semibold">[</span> <span class="fs-4 fw-semibold">${soNhoNhat}</span> <span class="text-danger fw-semibold">]</span>`;
  }
}
// END EX03

// START EX04
function timSoDuongNhoNhat() {
  var soDuongNhoNhat = 0;
  mainArray.forEach(function (n) {
    if (n > 0) {
      soDuongNhoNhat = n;
    }
  });
  if (soDuongNhoNhat == 0) {
    document.getElementById(
      "ex04-result"
    ).innerHTML = `Mảng <span class="text-warning fw-semibold">KHÔNG</span> có số DƯƠNG`;
  } else {
    mainArray.forEach(function (n) {
      if (n > 0 && soDuongNhoNhat > n) {
        soDuongNhoNhat = n;
      }
    });
    document.getElementById(
      "ex04-result"
    ).innerHTML = `Số <span class="text-warning fw-semibold">DƯƠNG</span> Nhỏ Nhất trong mảng là: <span class="text-danger fw-semibold">[</span> <span class="fs-4 fw-semibold">${soDuongNhoNhat}</span> <span class="text-danger fw-semibold">]</span>`;
  }
}
// END EX04

// START EX05
function timSoChanCuoi() {
  var soChanCuoi = -1;
  mainArray.forEach(function (n) {
    if (n % 2 == 0 && n != 0) {
      soChanCuoi = n;
    }
  });
  if (soChanCuoi == -1) {
    document.getElementById(
      "ex05-result"
    ).innerHTML = `Mảng <span class="text-warning fw-semibold">KHÔNG</span> có số CHẴN`;
  } else {
    document.getElementById(
      "ex05-result"
    ).innerHTML = `Số <span class="text-warning fw-semibold">CHẴN</span> cuối cùng trong mảng là: <span class="text-danger fw-semibold">[</span> <span class="fs-4 fw-semibold">${soChanCuoi}</span> <span class="text-danger fw-semibold">]</span>`;
  }
}
// END EX05

// START EX06
function doiCho2GiaTri() {
  var viTri1 = document.getElementById("ex06-viTri1").value * 1;
  var viTri2 = document.getElementById("ex06-viTri2").value * 1;

  // start changing order process
  if (viTri1 < 0 || viTri2 < 0) {
    document.getElementById(
      "ex06-result"
    ).innerHTML = `Giá trị của vị trí phải là <span class="text-warning fw-semibold">số Nguyên (lớn hơn hoặc bằng 0)</span>`;
  } else if (viTri1 > mainArray.length - 1 || viTri2 > mainArray.length - 1) {
    document.getElementById(
      "ex06-result"
    ).innerHTML = `Giá trị của vị trí phải <span class="text-warning fw-semibold">NHỎ HƠN Tổng số lượng phần tử trong mảng - 1</span>`;
  } else {
    var giaTriTrungGian = mainArray[viTri1];
    mainArray[viTri1] = mainArray[viTri2];
    mainArray[viTri2] = giaTriTrungGian;
    showArray(mainArray, mainArrayLocationId);
    // end changing order process

    // start showing result
    var changedArrayToShow = "";
    mainArray.forEach(function (element, index) {
      if (index === viTri1 || index === viTri2) {
        changedArrayToShow += `<span class="text-warning">${element}</span>, `;
      } else {
        changedArrayToShow += element + ", ";
      }
    });
    changedArrayToShow = changedArrayToShow.slice(0, -2);
    document.getElementById(
      "ex06-result"
    ).innerHTML = `<span class="text-danger">[</span> ${changedArrayToShow} <span class="text-danger">]</span>`;
  }
  // end showwing result
}
// END EX06

// START EX07
var accendedArray = [];

// start sorting in accending order
function arrangeAccending() {
  accendedArray = [];
  accendedArray = mainArray.map(function (element) {
    return element;
  });
  accendedArray.sort(function (a, b) {
    return a - b;
  });
  showArray(accendedArray, "ex07-result");
  document.getElementById("ex07-update").classList.remove("d-none");
  document.getElementById("ex07-reset").classList.remove("d-none");
}
// start sorting in accending order

// start modifying buttons
function updateArrayEx07() {
  mainArray = [];
  mainArray = accendedArray.map(function (element) {
    return element;
  });
  showArray(mainArray, mainArrayLocationId);
}
function resetArrayEx07() {
  accendedArray = mainArray.map(function (element) {
    return element;
  });
  showArray(accendedArray, "ex07-result");
}
// end modifying button
// END EX07

// START EX08
function findFirstPrimeNum() {
  // start check prime number function
  var checkPrimeNum = function (n) {
    if (n <= 1) {
      return false;
    } else if (n - Math.floor(n) != 0) {
      return false;
    } else {
      for (var count = 2; count <= Math.sqrt(n); count++) {
        if (n % count == 0) {
          return false;
        }
      }
    }
    return true;
  };
  // end check prime number function

  // start find first Prime number
  var firstPrimeNum = [];
  for (var index = 0; index < mainArray.length; index++) {
    if (checkPrimeNum(mainArray[index]) == true) {
      firstPrimeNum.push(mainArray[index], index);
      break;
    }
  }
  // end find first Prime number

  // start showing result
  switch (firstPrimeNum[0]) {
    case undefined:
      document.getElementById(
        "ex08-result"
      ).innerHTML = `Mảng <span class="text-warning fw-semibold">KHÔNG</span> có số Nguyên tố`;
      break;
    default:
      document.getElementById(
        "ex08-result"
      ).innerHTML = `Số NGUYÊN TỐ Đầu Tiên trong mảng là: <span class="text-danger fw-semibold">[</span> <span class="fs-4 fw-semibold">${firstPrimeNum[0]}</span> <span class="text-danger fw-semibold">]</span> tại vị trí <span class="text-warning fs-4 fw-semibold">${firstPrimeNum[1]}</span>`;
  }
  // end showing result
}
// END EX08

// START EX09
// start thêm Enter vào thành Click vào button
function addToArrayEx09() {
  var element = document.getElementById("ex09-so-thuc").value * 1;
  mainArray.push(element);
  document.getElementById("ex09-so-thuc").value = "";
  document.getElementById("ex09-so-thuc").placeholder = `Đã thêm [${element}]`;
  showArray(mainArray, mainArrayLocationId);
}
// end thêm Enter vào thành Click vào button

// start tìm số lượng số nguyên
function findIntegerAmount() {
  var integerArray = mainArray.filter(function (element) {
    return element - Math.floor(element) == 0;
  });
  if (integerArray.length == 0) {
    document.getElementById(
      "ex09-result1"
    ).innerHTML = `Mảng <span class="text-warning fs-4 fw-semibold">KHÔNG</span> có Số Nguyên`;
  } else {
    document.getElementById(
      "ex09-result1"
    ).innerHTML = `Trong mảng có: <span class="text-warning fw-semibold">${integerArray.length}</span> Số Nguyên`;
    showArray(integerArray, "ex09-result2");
  }
}
// end tìm số lượng số nguyên
// END EX09

// START EX10
function comparePosiNega() {
  var positiveArray = mainArray.filter(function (element) {
    return element > 0;
  });
  var negativeArray = mainArray.filter(function (element) {
    return element < 0;
  });
  var totalPositive = positiveArray.length;
  var totalNegative = negativeArray.length;
  if (totalPositive > totalNegative) {
    document.getElementById(
      "ex10-result1"
    ).innerHTML = `Số lượng: Số dương (<span class="text-warning fs-4 fw-semibold"> ${totalPositive} </span>) <span class="text-danger fs-4 fw-semibold">></span> Số âm (<span class="text-warning fs-4 fw-semibold"> ${totalNegative} </span>)`;
  } else if (totalPositive == totalNegative) {
    document.getElementById(
      "ex10-result1"
    ).innerHTML = `Số lượng: Số dương (<span class="text-warning fs-4 fw-semibold"> ${totalPositive} </span>) <span class="text-danger fs-4 fw-semibold">=</span> Số âm (<span class="text-warning fs-4 fw-semibold"> ${totalNegative} </span>)`;
  } else {
    document.getElementById(
      "ex10-result1"
    ).innerHTML = `Số lượng: Số dương (<span class="text-warning fs-4 fw-semibold"> ${totalPositive} </span>) <span class="text-danger fs-4 fw-semibold"><</span> Số âm (<span class="text-warning fs-4 fw-semibold"> ${totalNegative} </span>)`;
  }
  document.getElementById("ex10-result2-container").classList.remove("d-none");
  document.getElementById("ex10-result3-container").classList.remove("d-none");
  showArray(positiveArray, "ex10-result2");
  showArray(negativeArray, "ex10-result3");
}
// END EX10
